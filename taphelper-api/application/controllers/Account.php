<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }

    public function uploadResume()
    {
        
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 100;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        if ( ! $this->upload->do_upload('file'))
        {
                $error = array('error' => $this->upload->display_errors());

            
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());

                
        }
    }
    
    public function uploadProfilePic()
        {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 100;
            $config['max_width']            = 1024;
            $config['max_height']           = 768;

            if ( ! $this->upload->do_upload('file'))
            {
                    $error = array('error' => $this->upload->display_errors());

                
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());

                    
            }
    }



    public function userRegister()
    {

        $insertData = [
            "first_name" => clean_data(post("first_name")),
            "last_name" => clean_data(post("last_name")),
            "email_address" => post("email_address"),
            "contact_number" => post("contact_number"),
            "password" => md5(post("password")),
            "created_at" => date("y-m-d h:i:s"),
            "status" => "Activate",
            "role" => "User"
        ];  

        $query = $this->Crud_model->insert("users",$insertData);
    }

    public function employerRegister()
    {
        
        $insertData = [
            "first_name" => clean_data(post("first_name")),
            "middle_name" => clean_data(post("middle_name")),
            "last_name" => clean_data(post("last_name")),
            "suffix_name" => clean_data(post("suffix_name")),
            "company" => post("company"),
            "email_address" => post("email_address"),
            "contact_number" => post("contact_number"),
            "password" => md5(post("password")),
            "status" => "Activate",
            "created_at" => date("y-m-d h:i:s"),
            "role" => "Employer"
        ];  

        $query = $this->Crud_model->insert("users",$insertData);
    }

    public function changePassword()
    {
        $whereData = [
            "password" => post("oldpassword"),
            "id" => $this->session->userdata('id'),
        ];

        $query = $this->Crud_model->selectCount("users","id",$whereData);
        $sendData = [];

        $updateData = [
            "password" => post("password"),
        ];
        $whereData2 = [
            "password" => post("password"),
            "id" => $this->session->userdata('id'),
        ];

        if(post("password") == post("confirmPassword"))
        {
            if($query)
            {
                $query = $this->Crud_model->update("users",$updateData, $whereData2);

                $sendData["message"] = "Change Password Successful";
            }
            else{
                $sendData["message"] = "Old password is incorrect";
            }
        }
        else{
            $sendData["message"] = "Password Mismatched";
        }
        

        echo json_encode($sendData);
    }

    public function checkUnique()
    {
        $column = post("column");
        $data = post("data");
        $where =[
            $column => $data
        ];

        $query = $this->Crud_model->selectCount("users","id",$where = "");
    
        echo json_encode($query->result());
    }
    
    public function login()
    {
        $username = clean_data(post("username"));
        $password = md5(post("password"));

        $query = $this->crud_model->login($username,$password); 
    }

    public function logout()  
    {  
      session_destroy();
      $this->session->unset_userdata('role','id','full_name');  
      redirect(base_url() . 'Login');  
    } 

    

    
}
