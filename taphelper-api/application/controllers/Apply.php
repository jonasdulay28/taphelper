<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apply extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }


    public function index()
	{
        
		$this->load->view('template/jh_template.php');
    }

    public function applyEmployee()
    {
        $data = [];
        if($this->session->get('role') == "Employee")
        {
            $segment2 = decrypt(strtolower($this->uri->segment(2)));
            $where = [
                "users_id" => $segment2,
            ];
            $query = $this->Crud_model->select("apply","*",$where);
            $data = [
                "data" => $query->result()
            ];
        }
        else if($this->session->get('role') == "Employer")
        {
            $segment2 = decrypt(strtolower($this->uri->segment(2)));
            $where = [
                "jobs_id" => $segment2,
            ];
            $query = $this->Crud_model->select("apply","*",$where);
            $data = [
                "data" => $query->result()
            ];
        }
        

    }

    public function viewApply()
    {
        $update = [
            "status" => "Seen",
            "updated_at" => date("y-m-d h:i:s"),
        ];
        $where =[
            "id" => decrypt(post("id")),
            "status" => ""
       ];
        $query = $this->Crud_model->update("apply",$update,$where);
    }

    public function applyStatus()
    {
        $update = [
            "status" => post("status"),
            "updated_at" => date("y-m-d h:i:s"),
        ];
        $where =[
            "id" => decrypt(post("id")),
            "status" => ""
       ];
        $query = $this->Crud_model->update("apply",$update,$where);
        
    }

    public function cancelApply()
    {
       $update = [
           "status" => "Cancel",
           "updated_at" => date("y-m-d h:i:s"),
       ];

       $where =[
            "id" => decrypt(post("id")),
       ];
     
       $query = $this->Crud_model->update("apply",$update,$where);
    }
    

    
}
