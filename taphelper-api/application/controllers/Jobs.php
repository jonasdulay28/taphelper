<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model");
        
    }

    public function index(){
        $this->load->view('template/taphelper');
    }

    public function jobs()
    {
        $query = $this->crud_model->select("jobs","*",$where="");
        echo json_encode($query->result());
    }

    public function jobSearch()
    {
        $like = [
            "job_title" => $this->input->post("filter") == null ? null :  $this->input->post("filter"),
        ];
        $andLike = [
            "region" => $this->input->post("region") == null ? null :  $this->input->post("region"),
            "province" => $this->input->post("province") == null ? null :  $this->input->post("province"),
        ];
        $query = $this->Crud_model->filter("jobs","*",$like,$andLike);
        echo json_encode($query->result());
    }

    public function jobProfile()
    {
        $this->load->view('template/taphelper');
        
    }

    public function jobProfileid()
    {
        $id = $this->uri->segment(3);
        $where = [
            "id" => $id,
        ];
        $query = $this->Crud_model->select("jobs","*",$where);
        $data = [
            "data" => $query->result()
        ];
        echo json_encode($query->result());
    }

    public function post_job()
    {
        $data = [
            "job_title" => clean_data(post("job_title")),
            "company" => clean_data(post("company")),
            "job_type" => clean_data(post("job-type")),
            "region" => clean_data(post("region")),
            "province" => clean_data(post("province")),
            "city" => clean_data(post("city")),
            "job_description" => clean_data(post("job_description")),
    
         ];
    
         $query = $this->Crud_model->insert("job_post",$data);
    
        echo json_encode($data);
    }
    
   

    public function apply()
    {
       $insertData = [
           "users_id" => $this->session->userdata("id"),
           "jobs_id" => decrypt(strtolower($this->uri->segment(2))),
           "status" => "Process",
           "created_at" => date("y-m-d h:i:s"),
       ];

       $query = $this->Crud_model->insert("apply",$insertData);
    }

    

    
}
