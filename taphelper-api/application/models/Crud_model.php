<?php
class Crud_model extends CI_Model{


	public function test()
	{
		$query = $this->db->select("*")
						->from("jobs")
						->get();

		return $query->result();
	}
    public function login($username,$password)
	{	
		$query = $this->db->select('password')
						->from('user')
						->where('username =', $username)
						->get();
			$data = [];
			$userData = [];
			$password = $password;
		if($query->num_rows()>0){
			$testpassword;
			foreach($query->result() as $k){
				$testPassword = $k->password;
			}
			if($testPassword == $password){
				$query = $this->db->select('*')
								->from('user')
								->where('username =',$username)
								->where('password =', $password)
								->get();
				if($query->num_rows()>0){
					foreach($query->result() as $k)
					{
						$userData['fullname'] = $k->first_name." ".$k->middle_name." ".$k->last_name." ".$k->suffix_name;
						$userData['role'] = $k->role;
						$userData['username'] = $k->username;
						$userData['id'] = $k->id;
					}

                    $this->session->set_userdata($userData);
                    $userData['msg'] = "loginSuccess";
				
				}
			}
			else{
				$userData ['msg'] = "passwordError";
			
			}
		}
		else{
			$userData ['msg'] = "usernameError";
			
        }

        return $userData;
	}

    public function insert($table,$data)
	{
		$query = $this->db->insert($table,$data);
		
		if($query){
			return 'success';
		}
		
	}

    public function insertWithId($table,$data)
	{
		$query = $this->db->insert($table,$data);
		$last_id = $this->db->insert_id();
		
		return $last_id;
		
    }
    
    public function select($table,$select,$where = "")
	{
		$this->db->select($select)
						->from($table);
						if($where != "")
						{
							$this->db->where($where);
						}
		$query = $this->db->get();
		return $query;
    }

    public function selectCount($table,$select,$where = "")
	{
		$this->db->select($select)
						->from($table);
						if($where != "")
						{
							$this->db->where($where);
						}
		$query = $this->db->get();
		return $query->num_rows();
    }

    public function filter($table,$select,$like,$andLike)
	{
		$this->db->select($select)
						->from($table);
						if($like != "")
						{
							$this->db->like($like);
                        }
                        if($andLike!= "")
                        {
                            $this->db->like($andLike); 
                        }
		$query = $this->db->get();
		return $query;
    }
    
    public function update($table,$data,$where)
	{
		$query = $this->db->where($where)
						->update($table,$data);

		if($query){
			return $query;
		}
	}





}
?>