//Vue
import Vue from 'vue'
//router
import VueRouter from 'vue-router'
//components
import Admin_Dashboard from './Admin_Dashboard.vue'
import Suggestions from 'v-suggestions'
//import 'v-suggestions/dist/v-suggestions.css' // you can import the stylesheets also (optional)

window.Vue = Vue;
//axios
import VueAxios from 'vue-axios'
import axios from 'axios'
import Dashboard from './admin/Dashboard.vue'
import View_Users from './admin/users/View.vue'
import View_Workers from './admin/users/ViewWorkers.vue'
import View_Admin from './admin/users/ViewAdmin.vue'

import View_Feedbacks from './admin/feedbacks/View.vue'
import View_Jobs from './admin/jobs/View.vue'
import View_Categories from './admin/Categories/View.vue'
import ServicesPosted from './admin/Reports/ServicesPosted.vue'
import Household from './admin/Reports/Household.vue'
import Workers from './admin/Reports/Workers.vue'


View_Feedbacks

import Login from './admin/login/Login.vue'

import Navbar from './admin/Header.vue'
import Footer from './admin/Footer.vue'
import Sidebar from './admin/Sidebar.vue'
import VueCkeditor from 'vue-ckeditor2';
import DatatableFactory from 'vuejs-datatable';
import 'vue-datetime/dist/vue-datetime.css'

Vue.use(VueCkeditor);

window.axios = axios
Vue.use(VueAxios, axios)
Vue.use(VueRouter);
Vue.use(Suggestions);
Vue.use(DatatableFactory);
Vue.component('vue-ckeditor',VueCkeditor);
Vue.component('admin_header', Navbar);
Vue.component('admin_footer', Footer);
Vue.component('admin_sidebar', Sidebar);
Vue.component('admin_login', Login);
/*export const resumeBus = new Vue();*/

const routes = [
	{
		name: '/',
		path: '/',
		component: Dashboard
	},
	{
		name: '/view_owners',
		path: '/view_owners',
		component: View_Users
	},
	{
		name: '/services_posted',
		path: '/services_posted',
		component: ServicesPosted
	},
	{
		name: '/household',
		path: '/household',
		component: Household
	},
	{
		name: '/workers',
		path: '/workers',
		component: Workers
	},
	{
		name: '/view_workers',
		path: '/view_workers',
		component: View_Workers
	},
	{
		name: '/view_admin',
		path: '/view_admin',
		component: View_Admin
	},
	{
		name: '/view_feedbacks',
		path: '/view_feedbacks',
		component: View_Feedbacks
	},
	{
		name: '/view_jobs',
		path: '/view_jobs',
		component: View_Jobs
	},
	{
		name: '/view_job_category',
		path: '/view_job_category',
		component: View_Categories
	}
	// {
	// 	name: '/view_orders/:id',
	// 	path: '/view_orders/:id',
	// 	component: View_Orders
	// },
];
const router = new VueRouter({
	routes,
	linkExactActiveClass: "active" // active class for *exact* links.,
	//,mode: 'history'
});


new Vue({
    el: '#app',
    router,
    render: app => app(Admin_Dashboard)
});