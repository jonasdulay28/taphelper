import Vue from 'vue';
//import App from './App.vue'
import VueAxios from 'vue-axios';
import axios from 'axios';
//import CartComponent from './components/CartComponent.vue'
//import 'element-ui/lib/theme-chalk/index.css';
//ADMIN
// import Navbar_sidebar from './admin_components/Navbar_sidebar.vue';
// import Admin_footer from './admin_components/Admin_footer.vue';
// import Dashboard from './admin_components/Dashboard.vue';
//USER-EMPLOYER
import Navbar from './components/Navbar.vue';
import Authentication from './components/Authentication.vue';
import Login from './components/Login.vue';
import Register from './components/Register.vue';
import Application from './components/Application.vue';
import Job_application from './components/Job_application.vue';
import Jobs from './components/Jobs.vue';
import Job_post from './components/Job_post.vue';
import View_job from './components/View_job.vue';
import Employer_registration from './components/Employer_registration.vue';
import Employer_profile from './components/Employer_profile.vue';
import Job_profile from './components/Job_profile.vue';
import Applicant from './components/Applicant.vue';
import Bookmark from './components/Bookmark.vue';
import Jobapplicants from './components/Jobapplicants.vue';
import Home from './components/Home.vue';
import Tapfoot from './components/Tapfoot.vue';
import Job_list from './components/Job_list.vue';
import User_profile from './components/User_profile.vue';
import ElementUI from 'element-ui';
import './element-variables.scss';
import 'core-js/fn/string/includes';
import "core-js/es7/array";
import "core-js/es7/object";
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import VueAutosuggest from "vue-autosuggest";
import { DataTables, DataTablesServer } from 'vue-data-tables';
import VueDataTables from 'vue-data-tables';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';



//USER-EMPLOYER
Vue.component('main-navbar', Navbar);
Vue.component('application', Application);
Vue.component('authentication', Authentication);
Vue.component('login', Login);
Vue.component('register', Register);
Vue.component('job-application', Job_application);
Vue.component('view-job', View_job);
Vue.component('bookmark', Bookmark);
Vue.component('home', Home);
Vue.component('employer-registration', Employer_registration)
Vue.component('job-profile', Job_profile);
Vue.component('applicant', Applicant);
Vue.component('tapfoot', Tapfoot);
Vue.component('jobapplicants', Jobapplicants);
Vue.component('job-post', Job_post);
Vue.component('jobs', Jobs);
Vue.component('user-profile', User_profile);
Vue.component('employer-profile', Employer_profile);
Vue.component('job-list', Job_list);

//ADMIN
// Vue.component('navbar-sidebar', Navbar_sidebar);
// Vue.component('admin-footer', Admin_footer);
// Vue.component('dashboard', Dashboard);

Vue.use(VueAutosuggest);
locale.use(lang);
Vue.use(ElementUI);
// import DataTables and DataTablesServer separately
Vue.use(DataTables);
Vue.use(DataTablesServer);
// import DataTables and DataTablesServer together

Vue.use(VueDataTables);
window.axios = axios;

Vue.use(VueAxios, axios);

//Vue.component('sabai-cart', CartComponent);
const app = new Vue({}).$mount('#app');
