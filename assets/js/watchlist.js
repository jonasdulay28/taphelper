$(document).ready(function(){
	$(document).on('click', '.table-watchlist .remove_card', function(){
		var id = $(this).data('id');

		$.ajax({
			type: 'POST',
			url: base_url + 'api/remove_item_to_wishlist',
			data: {
				id: id
			},
			success: function(data){

			}
		});
		$('tr[data-id="'+ id +'"').remove();
		if($('.table-watchlist tbody tr').length == 0) {
			$('.watchlist-container').html('<center><p>Your watchlist is currently empty. <a href="'+ base_url +'">Click here to continue shopping.</a></p> </center>');
		}
	});

	$('.btn-confirm-watchlist').on('click', function(){
		$.ajax({
			type: 'POST',
			url: base_url + 'api/move_wishlist_to_cart',
			data: {
				id: ''
			},
			success: function(data){
				data = JSON.parse(data);
				cart_list = data.cart_list;

				bus.$emit('refresh_cart', cart_list);
				toastr.success('Success');
				$('#outOfStockModal').modal('hide');
			}
		});
	});

	$('.btn-watchlist-to-cart').on('click', function(){
		$.ajax({
			type: 'POST',
			url: base_url + 'api/check_wishlist',
			data: {
				id: ''
			},
	      dataType:"json",
			success: function(data){
				if(data.length == 0){
					$.ajax({
						type: 'POST',
						url: base_url + 'api/move_wishlist_to_cart',
						data: {
							id: ''
						},
						success: function(data){
							data = JSON.parse(data);
							cart_list = data.cart_list;

							bus.$emit('refresh_cart', cart_list);
							toastr.success('Success');
						}
					});
					return;
				}
				$('#outOfStockModal .table tbody').html('');
				var append_tr = '';

				data.forEach(function(el){
					append_tr += '<tr>';
					append_tr += '<td><span style="color:red">' + el.card_name + '</span></td>';
					append_tr += '<td>' + el.set_name + '</td>';
					append_tr += '</tr>';
				});

				$('#outOfStockModal .table tbody').html(append_tr);
				$('#outOfStockModal').modal('show');
			}
		});
	});
});