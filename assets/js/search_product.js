$(document).ready(function(){
	$('.search_form input[type="text"]').keyup(function(e){
		var key = e.which;
		if(key == 13)  // the enter key code
		{
		  $('.search_form button').trigger('click');
		  return false;  
		}
		$('.search_form .auto-complete-container ul').html('');
		var search_name = $(this).val();
		$.ajax({
			type: 'POST',
			url: base_url + 'api/get_suggestion',
			data: {
				name: search_name
			},
			success: function(data){
				data = JSON.parse(data);
				var disp = '';

				data.forEach(function(elem){
					disp += '<li><div>'+ elem.name +'</div></li>';
				});

				$('.search_form .auto-complete-container ul').html(disp);
			}
		})
	});

	$('.search_form input[type="text"]').focus(function(){
		$('.search_form .auto-complete-container').show();
	});

	$('html').on('click', function(e) {
		if($(e.target).closest('.search_form').length == 0) {
			$('.search_form .auto-complete-container').hide();
		}
	});

	$('.search_form button').click(function(){
		var search_text = $('.search_form').find('input[type="text"]').eq(1).val();
		window.location.href = base_url + 'cards/?disp=all&name=' + search_text;
	});

	$(document).on('click', '.auto-complete-container ul li', function(){
		var value = $(this).find('div').html();
		$('.search_form input[type="text"]').val(value);
		window.location.href = base_url + 'cards/?disp=all&name=' + value;
		$('.search_form .auto-complete-container ul').html('');
	});

});

function boldString(str, find){
    var re = new RegExp(find, 'g');
    return str.replace(re, '<b>'+find+'</b>');
}