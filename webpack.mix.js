let mix = require('laravel-mix');
mix.webpackConfig({
	resolve: {
		alias: {
			'vuejs-datatable': 'vuejs-datatable/dist/vuejs-datatable.esm.js'
		}
	},
});
mix.setPublicPath('');
mix.js('./assets/src/js/app.js', './assets/src/dist/js/app.js')
mix.js('./assets/src/js/admin.js', './assets/src/dist/js/admin.js')