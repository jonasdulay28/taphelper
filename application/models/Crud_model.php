<?php
class Crud_model extends CI_Model
{


	public function test()
	{
		$query = $this->db->select("*")
			->from("jobs")
			->get();

		return $query->result();
	}

	public function fetch($table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}

		$query = $this->db->get($table);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

	function get_pending_jobs() {
		$current_date = date('Y-m-d h:i:s');
		$query = $this->db->select('a.*,b.area_city,b.area_province,c.first_name,c.last_name,c.contact_number')
		->from('jobs a')
		->join('areas b', 'b.area_code = a.area_code','left')
		->join('users c',' c.id = a.user_id','left')
		->where('a.status','Pending')
		->where('a.worker_id IS NULL')
		->where('a.expiry >=',$current_date)
		->where('a.expiry <=','expiry')
		->order_by('a.created_at desc')
		->get();
		return $query->result();
	}

	public function get_all_jobs() {
		$query = $this->db->select('a.*,b.first_name as worker_fname,b.last_name as worker_lname,b.contact_number as worker_contact_number,c.first_name,c.last_name,c.contact_number')
		->from('jobs a')
		->join('users b',' b.id = a.worker_id','left')
		->join('users c',' c.id = a.user_id','left');
		$query = $this->db->order_by('FIELD(a.status, "Pending", "Confirmed", "On Process","Completed","Cancelled"),a.created_at desc')->get();
		return $query->result();
	}

	public function num_job_per_category() {
		$query = $this->db->select('count(a.id) as cnt_jobs, b.job_category')->from('jobs a')->join('job_categories b','b.job_category = a.job_title','left')
		->group_by('a.job_title')->get();
		return $query->result();
	}

	public function num_job_per_location() {
		$query = $this->db->select('count(a.id) as cnt_jobs,a.address, b.job_category')->from('jobs a')->join('job_categories b','b.job_category = a.job_title','left')
		->group_by('a.address')->get();
		return $query->result();
	}

	public function num_job_completed_per_location() {
		$query = $this->db->select('count(a.id) as cnt_jobs,a.address, b.job_category')->from('jobs a')->join('job_categories b','b.job_category = a.job_title','left')
		->where('a.status','Completed')
		->group_by('a.address')->get();
		return $query->result();
	}

	public function num_job_pending_per_location() {
		$query = $this->db->select('count(a.id) as cnt_jobs,a.address, b.job_category')->from('jobs a')->join('job_categories b','b.job_category = a.job_title','left')
		->where('a.status','Pending')
		->group_by('a.address')->get();
		return $query->result();
	}
	

	public function num_gender_worker() {
		$query = $this->db->select('count(id) as cnt_worker, gender')->from('users')
		->where('role','Worker')
		->group_by('gender')->get();
		return $query->result();
	}

	public function num_location_worker() {
		$query = $this->db->select('count(id) as cnt_worker, baranggay')->from('users')
		->where('role','Worker')
		->group_by('baranggay')->get();
		return $query->result();
	}

	public function num_location_household() {
		$query = $this->db->select('count(id) as cnt_household, baranggay')->from('users')
		->where('role','Employer')
		->group_by('baranggay')->get();
		return $query->result();
	}

	public function num_gender_household() {
		$query = $this->db->select('count(id) as cnt_household, gender')->from('users')
		->where('role','Employer')
		->group_by('gender')->get();
		return $query->result();
	}

	function get_my_jobs($user_id,$role) {
		$query = $this->db->select('a.*,b.area_city,b.area_province,c.first_name,c.last_name,c.contact_number')
		->from('jobs a')
		->join('areas b', 'b.area_code = a.area_code','left');
		
		if($role == "Worker") {
			$this->db->join('users c',' c.id = a.user_id','left')
			->where('a.worker_id',$user_id);
		} else{
			$this->db->join('users c',' c.id = a.worker_id','left')
			->where('a.user_id',$user_id);
		}
		$query = $this->db->order_by('FIELD(a.status, "Pending", "Confirmed", "On Process","Completed","Cancelled"),a.created_at desc')->get();
		return $query->result();
	}

	public function get_all_job_cat() {
		$query = $this->db->select('a.*, COUNT(b.id) as num_jobs')->from('job_categories a')
		->join('jobs b','b.job_title = a.job_category','left')
		->group_by('a.job_category')
		->order_by('a.job_category')
		->get();

		return $query->result();
	}


	
	public function get_job_details($job_key,$user_id) {
		$query = $this->db->select('a.*,b.area_city,b.area_province,c.first_name,c.last_name,c.contact_number')
		->from('jobs a')
		->join('areas b', 'b.area_code = a.area_code','left')
		->join('users c',' c.id = a.user_id','left')
		->where('a.job_key',$job_key)
		->where('a.worker_id',$user_id)
		->or_where('a.user_id',$user_id)
		->order_by('a.created_at desc')
		->get();
		return $query->result();
	}

	function check_exist($table, $filter)
	{
		return $this->db->select('id')->from($table)->where($filter)->get()->num_rows();
	}

	public function fetch_featured($table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		$this->db->order_by("id", "RANDOM");

		$query = $this->db->get($table);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function fetch_data($table, $where = "", $limit = "", $offset = "", $order = "", $group = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		if (!empty($group)) {
			$this->db->group_by($group);
		}

		$query = $this->db->get($table);
		return $query->result();
	}


	public function fetch_tag($tag, $table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$this->db->select($tag);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function fetch_tag_array($tag, $table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$this->db->select($tag);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return FALSE;
		}
	}


	public function fetch_tag_row($tag, $table, $where = "", $limit = "", $offset = "", $order = "")
	{
		if (!empty($where)) {
			$this->db->where($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$this->db->select($tag);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return FALSE;
		}
	}

	public function fetch_tag_row_like($tag, $table, $where = "", $limit = "", $offset = "", $order = "")
	{
		date_default_timezone_set('Asia/Manila');
		if (!empty($where)) {
			$this->db->like($where);
		}
		if (!empty($limit)) {
			if (!empty($offset)) {
				$this->db->limit($limit, $offset);
			} else {
				$this->db->limit($limit);
			}
		}
		if (!empty($order)) {
			$this->db->order_by($order);
		}
		$this->db->select($tag);
		$this->db->from($table);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return FALSE;
		}
	}


	public function insert($table, $data)
	{
		$query = $this->db->insert($table, $data);

		if ($query) {
			return 'success';
		}
	}

	public function insertWithId($table, $data)
	{
		$query = $this->db->insert($table, $data);
		$last_id = $this->db->insert_id();

		return $last_id;
	}

	public function select($table, $select, $where = "")
	{
		$this->db->select($select)
			->from($table);
		if ($where != "") {
			$this->db->where($where);
		}
		$this->db->order_by("id", "desc");
		$query = $this->db->get();
		return $query;
	}

	public function selectCount($table, $select, $where = "")
	{
		$this->db->select($select)
			->from($table);
		if ($where != "") {
			$this->db->where($where);
		}
		$query = $this->db->get();
		return $query->num_rows();
	}

	

	public function count_rows($table, $where = "")
	{
		$this->db->select('id')
			->from($table);
		if ($where != "") {
			$this->db->where($where);
		}
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function filter($table, $select, $like, $andLike)
	{
		$this->db->select($select)
			->from($table);
		if ($like != "") {
			$this->db->like($like);
		}
		if ($andLike != "") {
			$this->db->like($andLike);
		}
		$query = $this->db->get();
		return $query;
	}

	public function update($table, $data, $where)
	{
		$query = $this->db->where($where)
			->update($table, $data);

		if ($query) {
			return $query;
		}
	}

	public function getNotification($where)
	{
		$query = $this->db->select("first_name, middle_name, last_name, notification.users_id, notification.job_key, notification.id,notification, job_title, jobs.company, location, apply.status, notification.created_at")
						->from("notification")
						->join("jobs", "jobs.job_key = notification.job_key","left")
						->join("users", "users.id = notification.users_id", "left")
						->join("apply", "users.id = apply.users_id", "left")
						->where($where)
						->order_by("id", "DESC")
						->limit(10)
						->get();
		return $query;
	}

	
	public function notify($data)
	{
		$query = $this->db->insert("notification", $data);

		if ($query) {
			return 'success';
		}
	}

	// public function seenNotification()
	// {
	// 	$where = [
	// 		"id"  => $id,
	// 	];

	// 	$data = [
	// 		"status" => "Seen",
	// 	];
	// 	$query = $this->db->where($where)
	// 		->update($table, $data);

	// 	if ($query) {
	// 		return $query;
	// 	}
	// }

	// public function approveNotification($id)
	// {
		
	// }

	// public function rejectNotification()
	// {
		
	// }
		
	public function getAllApplicants($id)
	{
		$query = $this->db->select("apply.users_id, first_name, middle_name, last_name, suffix_name,offer, job_title, job_type, apply.id as applyId, apply.status as status")
						->from("apply")
						->join("jobs","jobs.job_key = apply.job_key", "left")
						->join("users",'users.id = apply.users_id','left')
						->where("jobs.created_by",$id)
						->get();
		
		return $query;
	}

	public function getSpecificJobApplicants($id,$job_key)
	{
		$query = $this->db->select("apply.users_id, first_name, middle_name, last_name,salary, job_title, job_type, apply.id as applyId")
						->from("jobs")
						->join("apply","jobs.job_key = apply.job_key", "left")
						->join("users",'users.id = apply.users_id','left')
						->where("jobs.job_key",$job_key)
						->where("jobs.created_by",$id)
						->get();
		
		return $query;
	}

	public function getAllApply($id)
	{
		$query = $this->db->select("apply.job_key, jobs.company,offer, job_title, apply.status as status, location")
						->from("apply")
						->join("jobs","jobs.job_key = apply.job_key", "left")
						->join("users",'users.id = apply.users_id','left')
						->where("apply.users_id",$id)
						->get();
		
		return $query;
	}


	public function bookmarks($where)
	{
		$query = $this->db->select("job_title, company, job_type, jobs.id, location, bookmark.job_key")
						->from("bookmark")
						->join("jobs", "jobs.job_key = bookmark.job_key", "left")
						->where($where)
						->get();
		return $query;

	}

	public function getEmployerInfo($job_key)
	{
		$query = $this->db->select("first_name, middle_name, last_name, contact_number, email_address, employer_location, employer_landline, profile_picture, created_by")
						->from("users")
						->join("jobs", "users.id = jobs.created_by", "left")
						->join("users_profile", "users.id = users_profile.users_id", "left")
						->where("job_key",$job_key)
						->get()->result();

		return $query;
	}


	public function getAllComment($where)
	{
		$query = $this->db->select("first_name, middle_name, last_name, comment, profile_picture,comment.id")
						->from("comment")
						->join("users", "users.id = comment.users_id", "left")
						->join("users_profile", "users.id = users_profile.users_id", "left")
						->where($where)
						->order_by("comment.created_at", "DESC")
						->limit(5)
						->get();
		return $query;
	}




	

	
}
