<?php
class User_model extends CI_Model
{
   public function get_user_details($filter) {
      $query = $this->db->select('*')->from('users')
      ->where($filter)
      ->get()->row();

      return $query;
   }
}