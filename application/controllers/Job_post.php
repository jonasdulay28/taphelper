<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') or exit('No direct script access allowed');

class job_post extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');

        $this->load->model("Crud_model");
    }

    public function index()
    {
        $this->load->view('template/job_post_template');
    }

    public function job_categories()
    {
        $data['job_categories'] = $this->Crud_model->fetch("job_categories");

        echo json_encode($data);
    }

    public function post_job()
    {
        $jobpost_data = (array) json_decode(post("jobpost_data"));
        $user_id = $this->session->user_id;
        $jobpost_data['user_id'] = $user_id;
        $check_exist  = 0;
        do {
            $jobpost_data['job_key'] = generateRandomString(5);
            $check_exist = $this->Crud_model->check_exist('jobs', ['job_key' => $jobpost_data['job_key']]);
        } while ($check_exist > 0);

        $tmp_prefer_date = $jobpost_data['prefer_date'];
        $prefer_date = "";
        if ((isset($tmp_prefer_date['isTrusted']) && $tmp_prefer_date['isTrusted'] == false) || $tmp_prefer_date == '') {
            $prefer_date = date('Y-m-d h:i:s');
        } else {
            $prefer_date = date('Y-m-d h:i:s', strtotime($tmp_prefer_date));
        }

        $jobpost_data['prefer_date'] = $prefer_date;

        $tmp_expiry = $jobpost_data['expiry'];
        $expiry = "";
        if ((isset($tmp_expiry['isTrusted']) && $tmp_expiry['isTrusted'] == false) || $tmp_expiry == '') {
            $expiry = date('Y-m-d h:i:s');
        } else {
            $expiry = date('Y-m-d h:i:s', strtotime($tmp_expiry));
        }

        $jobpost_data['expiry'] = $expiry;

        if (!empty($_FILES['file']['name'])) {
            // where are you storing it
            $path = './uploads';
            // what are you naming it
            $new_name = 'image-' . time();
            // start upload
            $input = 'file';
            $result = $this->upload_image($path, $new_name, $input);
            if ($result == "success") {
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $jobpost_data['image'] = "uploads/" . $new_name . "." . $ext;
            } else {
                $response["message"] = "error";
            }
        }

        $this->Crud_model->insert('jobs', $jobpost_data);
        $response["message"] = "success";
        $job_specialty = "%".$jobpost_data['job_title']."%";
        $workers = $this->Crud_model->fetch('users',['role'=>'Worker','job_specialty LIKE'=>$job_specialty]);
        if(!empty($workers)) {
            foreach($workers as $worker) {
                $contact_number = $worker->contact_number;
                sendSms($jobpost_data['job_title']." service is available in Taphelper. Accept the offer now!",$contact_number);
            }
        }
        
        echo json_encode($response);
    }


    function upload_image($path, $new_name, $input)
    {
        // define parameters
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $new_name;
        $this->load->library('upload');
        $this->upload->initialize($config);
        // upload the image
        if ($this->upload->do_upload($input)) {
            return "success";
        } else {
            return "failed";
        }
    }
}
