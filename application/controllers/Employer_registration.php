<?php
defined('BASEPATH') or exit('No direct script access allowed');

class employer_registration extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
        $this->load->model("Crud_model");
    }

    public function index()
    {
        $this->load->view('template/employer_registration_template');
    }

   

    

    public function employerRegister()
    {

        $insertData = [
            "first_name" => clean_data(post("first_name")),
            "middle_name" => clean_data(post("middle_name")),
            "last_name" => clean_data(post("last_name")),
            "suffix_name" => clean_data(post("suffix_name")),
            "company" => post("company"),
            "email_address" => post("email_address"),
            "contact_number" => post("contact_number"),
            "password" => md5(post("password")),
            "status" => "Activate",
            "created_at" => date("y-m-d h:i:s"),
            "role" => "Employer"
        ];

        $query = $this->Crud_model->insert("users", $insertData);
    }

    

    public function checkUnique()
    {
        $column = post("column");
        $data = post("data");
        $where = [
            $column => $data
        ];

        $query = $this->Crud_model->selectCount("users", "id", $where = "");

        echo json_encode($query->result());
    }

   
}
