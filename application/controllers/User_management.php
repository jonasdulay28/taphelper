<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') OR exit('No direct script access allowed');

class user_management extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }


    public function index()
	{
        $this->load->view('template/user_management_template.php
        ');
    }


    public function get_all_user()
    {
        $query = $this->Crud_model->select("users","*",$where="");
        echo json_encode($query->result());
    }

    public function get_all_owner()
    {
        $data['users'] = $this->Crud_model->fetch('users',['role'=>"Employer"]);
        echo json_encode($data);
    }

    public function get_all_workers()
    {
        $data['users'] = $this->Crud_model->fetch('users',['role'=>"Worker"]);
        echo json_encode($data);
    }

    public function get_all_admins()
    {
        $data['users'] = $this->Crud_model->fetch('users',['role'=>"Admin"]);
        echo json_encode($data);
    }

    public function viewUser()
    {   
        $filter = [
            "a.id" => post("id"),
        ];
        $this->load->model('User_model');
        $data['users_details'] = $this->User_model->get_user_details($filter);
        echo json_encode($data);
    }

    public function changeStatus()
    {
        $where = [
            "id" =>  post('id'),
        ];
        $status = post('status') == "Active" ? "Inactive" : "Active";
        $update = ["status"=>$status];

        $this->Crud_model->update("users", $update, $where);
        $msg["message"] = "Update Successfuly.";

        echo json_encode($msg);
    }

    public function edit_user()
    {
        $where = [
            "id" => post("id")
        ];
        $query = $this->Crud_model->select("users","*",$where);
        
        echo json_encode($query->result());
    }

    public function update_user()
    {
        $updata = post('user_profile');

        $usersData = [
            // "first_name" => $updata['first_name'],
            // "middle_name" => $updata['middle_name'],
            // "last_name" => $updata['last_name'],
            // "suffix_name" => $updata['suffix_name'],
            // "contact_number" => $updata['contact_number'],
            // "email_address" => $updata['email_address'],
            // "company" => $updata["company"],
            "password" => $update["password"]
        ];

        $where = [
            "id" => $this->session->user_id,
        ];
        $this->Crud_model->update("users", $usersData, $where);
        $msg["message"] = "Update Successfuly.";

        echo json_encode($msg);
    }
    
}
