<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bookmark extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }


    public function index()
	{
        
		$this->load->view('template/bookmark_template.php');
    }

   
    public function getMyBookmarks()
    {

            $where = [
                "users_id" => $this->session->user_id,
                "bookmark.status" => "Active"
            ];
            $query = $this->Crud_model->bookmarks($where);
            $data=[];
            foreach($query->result() as $k)
            {
                $data[]= array(
                    "company" => $k->company ,
                    "job_title" =>  $k->job_title,
                    "job_type" =>  $k->job_type,
                    "location" => $k->location,
                    "id" => encrypt($k->id),
                    "job_key" => encrypt($k->job_key),
                );
            }

            echo json_encode ($data);
    }

    
}
