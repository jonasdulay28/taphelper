<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') or exit('No direct script access allowed');

class job_profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');

        $this->load->model("Crud_model");
    }

    public function index()
    {

        $job_key = get("job");
        $data['job_key'] = $job_key;
        $this->load->view('template/job_profile_template', $data);
    }

    public function get_job_details()
    {
        $user_id = isset($this->session->user_id) ? $this->session->user_id : "";
        $user_role = isset($this->session->role) ? $this->session->role : "";

        $job_key = decrypt(post("job_key"));
        $where = [
            "job_key" => $job_key,
        ];
        $data['job_details'] = $this->Crud_model->fetch_tag_row("*", "jobs", $where);
        $data['is_applied'] = false;
        $data['role'] = $user_role;
        if ($user_id != "") {
            $where = [
                "job_key" => $job_key,
                "users_id" => $user_id,
                "status" => "Processing"
            ];

            $row = $this->Crud_model->check_exist("apply", $where);
            $data['is_applied'] = $row > 0 ? true : false;
        }

        echo json_encode($data);
    }


    public function getEmployerInfo()
    {
        $job_key = clean_data(decrypt(post("job_key")));

        $data['employer_info'] = $this->Crud_model->getEmployerInfo($job_key);
        
        echo json_encode($data);
    }
    
    public function apply()
    {
        $job_key = clean_data(decrypt(post("job_key")));
        $user_id = $this->session->user_id;

        $insertData = [
            "users_id" => $user_id,
            "job_key" => $job_key,
            "status" => "Processing",
        ];
        $filter = [
            "users_id" => $user_id,
            "job_key" => $job_key,
        ];
        $msg["message"] = "Thank you for applying with us";

        $check = $this->Crud_model->check_exist('apply', $filter);
        if ($check  < 1) {
            $query = $this->Crud_model->insert("apply", $insertData);

            $where = [
                "job_key" => $job_key,
            ];
            $query = $this->Crud_model->select("jobs","created_by",$where);
            $employer_id = "";
            foreach($query->result() as $k)
            {
                $employer_id = $k->created_by;
            }
            $notifyData = [
                "users_id" => $user_id,
                "job_key" => $job_key,
                "target_user" => $employer_id,
                "notification" => "New Applicant",
                "status" => "New"
            ];
            $notify = $this->Crud_model->notify($notifyData);

        } else {
            $data = [
                "status" => "Processing"
            ];
            $this->Crud_model->update("apply", $data, $filter);
        }

        echo json_encode($msg);
    }


    public function job_profile_update()
    {
        $jobCurrentData = json_decode(post("jobCurrentData"));
      
        //gamitin mo ung job_key pang update
        $job_key = post("job_key");
        $updateData = [
            "job_title" =>  $jobCurrentData->job_title,
            "company" =>  $jobCurrentData->company,
            "job_type" =>  $jobCurrentData->job_type,
            "job_category" =>  $jobCurrentData->job_category,
            "job_description" =>  $jobCurrentData->job_description,
            "location" =>  $jobCurrentData->location,
            "salary" =>  $jobCurrentData->salary,
        ];

        $whereData = [
            "job_key" => decrypt($job_key),
        ];

        $query = $this->Crud_model->update("jobs",$updateData,$whereData);
       
 
        if (!empty($_FILES['file']['name'])) {
            // where are you storing it
            $path = './uploads';
            // what are you naming it
            $new_name = 'profpic-' . time();
            // start upload
            $input = 'file';
            $result = $this->upload_image($path, $new_name, $input);
            if ($result == "success") {
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $product_data['image'] = base_url() . "uploads/" . $new_name . "." . $ext;
                $updata = [
                    "job_profile_picture" => $new_name. "." .$ext,
                ];
                $whereData = [

                    "job_key" => decrypt($job_key),
                ];
               
                $query = $this->Crud_model->update("jobs",$updata,$whereData);
            } else {
                $response["message"] = "failed2";
            }
        } else {
            $response["message"] = "failedd";
        }

        $data["message"] = "Update Successfuly.";

        echo json_encode($data);
    }

    public function uploadProfilePic()
    {
        $response = ["message" => "success"];
        


        echo json_encode($response);
    }

    function upload_image($path, $new_name, $input)
    {
        // define parameters
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $new_name;
        $this->load->library('upload');
        $this->upload->initialize($config);
        // upload the image
        if ($this->upload->do_upload($input)) {
            return "success";
        } else {
            return "failed";
        }
    }

    public function cancel_application()
    {
        $job_key = decrypt(post("job_key"));
        $user_id = $this->session->user_id;
        $filter = [
            "users_id" => $user_id,
            "job_key" => $job_key,
        ];
        $msg["message"] = "Sorry to know that you cancelled your application.";
        $row = $this->Crud_model->fetch_tag_row("id", "apply", $filter);
        if ($row) {

            $data = [
                "status" => "Cancelled"
            ];
            $this->Crud_model->update("apply", $data, $filter);

            $where = [
                "job_key" => $job_key,
            ];
            $query = $this->Crud_model->select("jobs","created_by",$where);
            $employer_id = "";
            foreach($query->result() as $k)
            {
                $employer_id = $k->created_by;
            }
            $notifyData = [
                "users_id" => $user_id,
                "job_key" => $job_key,
                "target_user" => $employer_id,
                "notification" => "Cancel Application",
                "status" => "New"
            ];
            $notify = $this->Crud_model->notify($notifyData);
        }
        echo json_encode($msg);
    }

    public function comment()
    {  
        if (isset($this->session->user_id)) {
            
            $data = [
                "job_key" => decrypt(post("job_key")),
                "comment" => clean_data(post("comment")),
                "users_id" => $this->session->user_id,
            ];
            $query = $this->Crud_model->insert("comment",$data);

            echo json_encode(["message" => "success"]);
            
        } 
       
    }

    public function getComments()
    {
        $where = [
            "job_key" => decrypt(post("job_key")),
        ];
        $query = $this->Crud_model->getAllComment($where);

        echo json_encode($query->result());
    }
}
