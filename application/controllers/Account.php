<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') or exit('No direct script access allowed');

class account extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
        $this->load->model("Crud_model");
    }



    public function get_user()
    {
        $data = [];
        if (isset($this->session->user_id)) {
            $user_id = $this->session->user_id;
            $data = $this->Crud_model->fetch_tag_row('*', 'users', ['id' => $user_id]);
        }
        echo json_encode(['user_data' => $data]);
    }

    public function user_profile()
    {
        $data = [];
        if (isset($this->session->user_id)) {
            $user_id = $this->session->user_id;
            $data = $this->Crud_model->select('users_profile', '*', ['users_id' => $user_id]);
        }
        echo json_encode(['user_data' => $data->row()]);
    }


    public function userRegistration()
    {
        $response = ["message" => "success"];
        $registration_data = (array) json_decode(post('registration_data'));

        $insertData = [
            "first_name" => clean_data($registration_data["first_name"]),
            "middle_name" => clean_data($registration_data["middle_name"]),
            "last_name" => clean_data($registration_data["last_name"]),
            "email_address" => $registration_data["email"],
            "job_specialty" => $registration_data["job_specialty"],
            "gender" => $registration_data["gender"],
            "address" => $registration_data["address"],
            "baranggay" => $registration_data["baranggay"],
            "contact_number" => $registration_data["contact_number"],
            "password" => md5(clean_data($registration_data["password"])),
            "status" => "Active",
            "role" => clean_data($registration_data["role"]),
        ];

        if ($registration_data["role"] == "Worker") {
            $insertData["status"] = "Inactive";
        }
        $check_exist = $this->Crud_model->check_exist('users', ['email_address' => $registration_data["email"]]);
        if ($check_exist > 0) {
            $response = ["message" => "Email already exists"];
        } else {
            $this->Crud_model->insert('users', $insertData);
            $row = $this->Crud_model->fetch_tag_row('id,email_address', 'users', ['email_address' => $registration_data["email"]]);
            if ($registration_data['role'] == 'Employer') {
                $userData["user_id"] = $row->id;
                $userData["email_address"] = $row->email_address;
                $this->session->set_userdata($userData);
            }
        }
        $tmp_role = $registration_data["role"] == "Worker" ? "Worker" : "Household Owner";
        sendSms("A " . $tmp_role . " registered to taphelper. Check it now!", "09951984293");

        echo json_encode($response);
    }

    public function test_sms() {
        echo sendSMS("asd","09206008945");
    }

    public function adminRegistration()
    {
        $response = ["message" => "success"];
        $registration_data = post('registration_data');

        $insertData = [
            "first_name" => clean_data($registration_data["first_name"]),
            "middle_name" => clean_data($registration_data["middle_name"]),
            "last_name" => clean_data($registration_data["last_name"]),
            "email_address" => $registration_data["email"],
            "address" => $registration_data["address"],
            "contact_number" => $registration_data["contact_number"],
            "password" => md5(clean_data($registration_data["password"])),
            "status" => "Active",
            "role" => clean_data($registration_data["role"]),
        ];

        if ($registration_data["role"] == "Worker") {
            $insertData["status"] = "Inactive";
        }
        $check_exist = $this->Crud_model->check_exist('users', ['email_address' => $registration_data["email"]]);
        if ($check_exist > 0) {
            $response = ["message" => "Email already exists"];
        } else {
            $this->Crud_model->insert('users', $insertData);
            $row = $this->Crud_model->fetch_tag_row('id,email_address', 'users', ['email_address' => $registration_data["email"]]);
            $userData["user_id"] = $row->id;
            $userData["email_address"] = $row->email_address;
            $this->session->set_userdata($userData);
        }


        echo json_encode($response);
    }

    function upload_file($path, $new_name, $input, $type)
    {
        // define parameters
        $config['upload_path'] = $path;
        $config['allowed_types'] = $type;
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $new_name;
        $this->load->library('upload');
        $this->upload->initialize($config);
        // upload the image
        if ($this->upload->do_upload($input)) {
            return "success";
        } else {
            return "failed";
        }
    }

    public function checkUnique()
    {
        $column = post("column");
        $data = post("data");
        $where = [
            $column => $data
        ];

        $query = $this->Crud_model->selectCount("users", "id", $where = "");

        echo json_encode($query->result());
    }

    public function login()
    {
        $response = ["message" => "success"];
        $login_data = post("login_data");
        $username = clean_data($login_data["email"]);
        $password = md5($login_data["password"]);

        $row = $this->Crud_model->fetch_tag_row('id,password,status,role', 'users', ['email_address' => $username]);
        if (!empty($row)) {
            if ($row->status == "Active") {
                $current_password = $row->password;
                if ($current_password == $password) {
                    $userData["user_id"] = $row->id;
                    $userData["email_address"] = $username;
                    $this->session->set_userdata($userData);
                } else {
                    $response['message'] = "Invalid credentials";
                }
            } else {
                $response['message'] = "Invalid credentials";
            }
        } else {
            $response['message'] = "Invalid credentials";
        }

        echo json_encode($response);
    }

    public function adminLogin()
    {
        $response = ["message" => "success"];
        $login_data = post("login_data");
        $username = clean_data($login_data["email"]);
        $password = md5($login_data["password"]);

        $row = $this->Crud_model->fetch_tag_row('id,password,status,role', 'users', ['email_address' => $username]);
        if (!empty($row)) {
            if ($row->status == "Active" && $row->role == "Admin") {
                $current_password = $row->password;
                if ($current_password == $password) {
                    $userData["user_id"] = $row->id;
                    $userData["email_address"] = $username;
                    $this->session->set_userdata($userData);
                } else {
                    $response['message'] = "Invalid credentials";
                }
            } else {
                $response['message'] = "Invalid credentials";
            }
        } else {
            $response['message'] = "Invalid credentials";
        }

        echo json_encode($response);
    }

    public function getNotification()
    {
        if (isset($this->session->user_id)) {
            $where = [
                "target_user" => $this->session->user_id,
            ];
            $query = $this->Crud_model->getNotification($where);
            $data = [];
            foreach ($query->result() as $k) {
                $data[] = array(
                    "full_name" => $k->first_name . ' ' . $k->middle_name = null ? "" : $k->middle_name . ' ' . $k->last_name,
                    "notification" => $k->notification,
                    "job_title" => $k->job_title,
                    "id" => encrypt($k->id),
                    "date" => $k->created_at,

                );
            }
            echo json_encode($data);
        }
    }

    public function logout()
    {
        session_destroy();
        //$this->session->unset_userdata('role', 'id', 'full_name');
        redirect(base_url());
    }
}
