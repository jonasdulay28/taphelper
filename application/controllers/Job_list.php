<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') OR exit('No direct script access allowed');

class job_list extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }


    public function index()
	{
        
		$this->load->view('template/job_list_template.php');
    }

   
    public function getJobList()
    {

            $where = [
                "created_by" => $this->session->user_id,
            ];
            $query = $this->Crud_model->select("jobs","company,job_title,job_type,location,id,job_key,salary",$where);
            $data=[];
            foreach($query->result() as $k)
            {
                $data[]= array(
                    "company" => $k->company ,
                    "job_title" =>  $k->job_title,
                    "job_type" =>  $k->job_type,
                    "salary" =>  $k->salary,
                    "location" => $k->location,
                    "id" => encrypt($k->id),
                    "job_key" => encrypt($k->job_key),
                );
            }

            echo json_encode ($data);
    }



    
}
