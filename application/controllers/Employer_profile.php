<?php
defined('BASEPATH') or exit('No direct script access allowed');

class employer_profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
        $this->load->model("Crud_model");
    }

    public function index()
    {
        $this->load->view('template/employer_profile_template');
    }

    public function uploadResume()
    {

        $response = ["message" => "success"];
        if (!empty($_FILES['file']['name'])) {
            // where are you storing it
            $path = './resume';
            // what are you naming it
            $new_name = 'resume-' . time();
            // start upload
            $input = 'file';
            $result = $this->upload_file($path, $new_name, $input);
            if ($result == "success") {
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $product_data['image'] = base_url() . "resume/" . $new_name . "." . $ext;
                $updata = [
                    "resume" => $new_name. "." .$ext,
                ];
                $where = [
                    "users_id" => $this->session->user_id,
                ];
                $query = $this->Crud_model->update("users_profile",$updata,$where);
            } else {
                $response["message"] = "failed2";
            }
        } else {
            $response["message"] = "failedd";
        }


        echo json_encode($response);
    }

    public function uploadProfilePic()
    {
        $response = ["message" => "success"];
        if (!empty($_FILES['file']['name'])) {
            // where are you storing it
            $path = './uploads';
            // what are you naming it
            $new_name = 'profpic-' . time();
            // start upload
            $input = 'file';
            $result = $this->upload_image($path, $new_name, $input);
            if ($result == "success") {
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $product_data['image'] = base_url() . "uploads/" . $new_name . "." . $ext;
                $updata = [
                    "profile_picture" => $new_name. "." .$ext,
                ];
                $where = [
                    "users_id" => $this->session->user_id,
                ];
                $query = $this->Crud_model->update("users_profile",$updata,$where);
            } else {
                $response["message"] = "failed2";
            }
        } else {
            $response["message"] = "failedd";
        }


        echo json_encode($response);
    }

    function upload_file($path, $new_name, $input)
    {
        // define parameters
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $new_name;
        $this->load->library('upload');
        $this->upload->initialize($config);
        // upload the image
        if ($this->upload->do_upload($input)) {
            return "success";
        } else {
            return "failed";
        }
    }

    function upload_image($path, $new_name, $input)
    {
        // define parameters
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $new_name;
        $this->load->library('upload');
        $this->upload->initialize($config);
        // upload the image
        if ($this->upload->do_upload($input)) {
            return "success";
        } else {
            return "failed";
        }
    }

    public function updateData()
    {
        $update_data = post("update_userData");

        $updateData = [
            "first_name" => clean_data($update_data["first_name"]),
            "middle_name" => clean_data($update_data["middle_name"]),
            "last_name" => clean_data($update_data["last_name"]),
            "email_address" => $update_data["email"],
            "contact_number" => $update_data["contact_number"],
            "email_address" => $update_data["email_address"],

        ];
        $updateData2 = [
            "educational_attainment" => $update_data["educational_attainment"],
            "description" =>  $update_data["description"],
        ];

        $whereData2 = [
            "id" => $updateData["id"],
        ];

        $whereData3 = [
            "users_id" => $updateData["id"],
        ];

        $where = [
            "email_address" => $registration_data["email"],
        ];

        $data = [];
        $query = $this->Crud_model->select("users", "id", $where);
        if ($query->num_rows() > 0) {
            $data["message"] = "Email address is already taken.";
        } else {
            $query = $this->Crud_model->update("users", $updateData, $whereData2);
            $query = $this->Crud_model->update("users_profile", $updateData2, $whereData3);
            $data["message"] = "Successfuly Updated";
        }

        echo json_encode($data);
    }


    public function getAllUserData()
    {
        $filter = [
            "a.id" => $this->session->user_id,
        ];
        $this->load->model('User_model');
        $data['users_details'] = $this->User_model->get_user_details($filter);
        echo json_encode($data);
    }

    public function update_user_profile()
    {
        $updata = post('user_profile');

        $usersData = [
            "first_name" => $updata['first_name'],
            "middle_name" => $updata['middle_name'],
            "last_name" => $updata['last_name'],
            "suffix_name" => $updata['suffix_name'],
            "contact_number" => $updata['contact_number'],
            "email_address" => $updata['email_address'],
        ];

        $usersProfileData = [
            "description" => $updata["description"],
            "educational_attainment" => $updata["educational_attainment"],

        ];
        $where = [
            "id" => $this->session->user_id,
        ];
        $where2 = [
            "users_id" => $this->session->user_id,
        ];

        $this->Crud_model->update("users", $usersData, $where);
        $this->Crud_model->update("users_profile", $usersProfileData, $where2);
        $msg["message"] = "Update Successfuly.";

        echo json_encode($msg);
    }
}
