<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') or exit('No direct script access allowed');

class secure_admin extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
   }

   public function index()
   {
         $this->load->view('template/admin_template');
      
   }

   // public function authentication()
   // {
   //    if (is_logged_admin()) {
   //       redirect(base_url('secure_admin'));
   //    } else {
   //       $this->load->view('templates/admin_login_template');
   //    }
   // }

   // public function login()
   // {
   //    $this->load->model("User_model");
   //    $response = array("message" => "Invalid email or password.");
   //    $this->form_validation->set_rules('email', 'Email', 'required|trim');
   //    $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|trim');
   //    if ($this->form_validation->run() == TRUE) {
   //       $email = strtolower(clean_data(post('email')));
   //       $password =  clean_data(post('password'));
   //       $filter = array("email" => $email, "status" => "1");
   //       $user_details = $this->User_model->fetch_tag_row("password,role", "users", $filter);
   //       if (empty($user_details)) {
   //          $response["message"] = "Invalid email or password.";
   //       } else {
   //          $user_password = $user_details->password;
   //          $user_role = $user_details->role;
   //          if ($user_role == "admin") {
   //             if (password_verify($password, $user_password)) {
   //                $response["message"] = 'success';
   //                $response["url"] = base_url('secure_admin');
   //                $ses_key = encrypt($email);
   //                $sessdata = array('ses_key'  => $ses_key);
   //                $this->session->set_userdata($sessdata);
   //             }
   //          } else {
   //             $response["message"] = "Invalid email or password.";
   //          }
   //       }
   //    } else {
   //       $response["message"] = "Invalid email or password.";
   //    }
   //    echo json_encode($response);
   // }

   public function logout()
   {
      session_destroy();
      //audit("logout", "admin", "authentication", "");
      $data["url"] = base_url('secure_admin');
      echo json_encode($data);
   }
}
