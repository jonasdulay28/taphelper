<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') OR exit('No direct script access allowed');

class job_management extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }


    public function index()
	{
        $this->load->view('template/job_management_template.php
        ');
    }


    public function get_all_job()
    {
        $data['jobs'] = $this->Crud_model->get_all_jobs();

        echo json_encode($data);

    }


    public function get_all_job_categories() {
       $data['job_categories'] =  $this->Crud_model->get_all_job_cat();
       echo json_encode($data);
    }

    public function changeStatus()
    {
        $where = [
            "id" =>  post('id'),
        ];
        $update = [];
        if(post("status") == "Active")
        {
            $update = [
                "status" => "Inactive",
            ];
        }
        else{
            $update = [
                "status" => "Active",
            ];
        }

        $this->Crud_model->update("jobs", $update, $where);
        $msg["message"] = "Update Successfuly.";

        echo json_encode($msg);
    }

    public function update_job()
    {
        $jobCurrentData = json_decode(post("jobCurrentData"));
      
        //gamitin mo ung job_key pang update
        $job_key = post("job_key");
        $updateData = [
            "job_title" =>  $jobCurrentData->job_title,
            "company" =>  $jobCurrentData->company,
            "job_type" =>  $jobCurrentData->job_type,
            "job_category" =>  $jobCurrentData->job_category,
            "job_description" =>  $jobCurrentData->job_description,
            "location" =>  $jobCurrentData->location,
            "salary" =>  $jobCurrentData->salary,
        ];

        $whereData = [
            "job_key" => decrypt($job_key),
        ];

        $query = $this->Crud_model->update("jobs",$updateData,$whereData);
       
 
        if (!empty($_FILES['file']['name'])) {
            // where are you storing it
            $path = './uploads';
            // what are you naming it
            $new_name = 'profpic-' . time();
            // start upload
            $input = 'file';
            $result = $this->upload_image($path, $new_name, $input);
            if ($result == "success") {
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $product_data['image'] = base_url() . "uploads/" . $new_name . "." . $ext;
                $updata = [
                    "job_profile_picture" => $new_name. "." .$ext,
                ];
                $whereData = [
                    "job_key" => decrypt($job_key),
                ];
               
                $query = $this->Crud_model->update("jobs",$updata,$whereData);
            } else {
                $response["message"] = "failed2";
            }
        } else {
            $response["message"] = "failedd";
        }

        $data["message"] = "Update Successfuly.";

        echo json_encode($data);
    }

    public function deactivate_job()
    {
        $job_key = "";
        $update = [
            "status" => "Deactivate",
        ];

        $where = [
            "job_key" => decrypt($job_key)
        ];

        $query = $this->Crud_model->update("jobs",$update,$where);

        $data["message"] = "Deactivate Successfuly.";

        echo json_encode($data);
    }

    function upload_image($path, $new_name, $input)
    {
        // define parameters
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $new_name;
        $this->load->library('upload');
        $this->upload->initialize($config);
        // upload the image
        if ($this->upload->do_upload($input)) {
            return "success";
        } else {
            return "failed";
        }
    }
    
}
