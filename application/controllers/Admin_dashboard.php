<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }


    public function getdashboard()
    {
        $data['users_household'] = $this->Crud_model->count_rows("users",["role"=>"Employer"]);
        $data['users_workers'] = $this->Crud_model->count_rows("users",["role"=>"Worker"]);
        $data['jobs'] = $this->Crud_model->count_rows("jobs");
        $data['job_categories'] = $this->Crud_model->count_rows("job_categories");
        $data['num_job_per_category'] = $this->Crud_model->num_job_per_category();
        $data['num_gender_worker'] = $this->Crud_model->num_gender_worker();
        $data['num_gender_household'] = $this->Crud_model->num_gender_household();
        $data['num_job_per_location'] = $this->Crud_model->num_job_per_location();
        echo json_encode($data);
    }

    public function getUserReport() {
        $data['num_location_workers'] = $this->Crud_model->num_location_worker();
        $data['num_location_household'] = $this->Crud_model->num_location_household();
        echo json_encode($data);
    }

    public function getServicesData() {
        $data['num_job_per_location'] = $this->Crud_model->num_job_per_location();
        $data['num_job_completed_per_location'] = $this->Crud_model->num_job_completed_per_location();
        $data['num_job_pending_per_location'] = $this->Crud_model->num_job_pending_per_location();
        $data['num_job_per_category'] = $this->Crud_model->num_job_per_category();


        echo json_encode($data);
    }
    
}
