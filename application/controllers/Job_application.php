<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') OR exit('No direct script access allowed');

class job_application extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }


    public function index()
	{
        
		$this->load->view('template/job_application_template.php');
    }

   
    public function getMyApply()
    {
        $user_id = $this->session->user_id;
            $query = $this->Crud_model->getAllApply($user_id);
            $data=[];
            foreach($query->result() as $k)
            {
                $data[]= array(
                    "company" => $k->company ,
                    "job_title" =>  $k->job_title,
                    "status" =>  $k->status,
                    "location" => $k->location,
                    "id" => encrypt($k->job_key),
                );
            }

            echo json_encode ($data);
    }

    
}
