<?php
defined('BASEPATH') or exit('No direct script access allowed');

class jobapplication extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');

        $this->load->model("Crud_model");
    }


    public function index()
    {

        $this->load->view('template/application_template');
    }

    public function applyEmployee()
    {
        $data = [];
        if ($this->session->get('role') == "Employee") {
            $segment2 = decrypt(strtolower($this->uri->segment(2)));
            $where = [
                "users_id" => $segment2,
            ];
            $query = $this->Crud_model->select("apply", "*", $where);
            $data = [
                "data" => $query->result()
            ];
        } else if ($this->session->get('role') == "Employer") {
            $segment2 = decrypt(strtolower($this->uri->segment(2)));
            $where = [
                "jobs_id" => $segment2,
            ];
            $query = $this->Crud_model->select("apply", "*", $where);
            $data = [
                "data" => $query->result()
            ];
        }
    }

    public function applicant()
    {
        // $data['job_key'] = $this->input->get("job");
        $this->load->view('template/applicants_template');
    }

    public function get_applicant_data()
    {
        $filter = [
            "a.id" => decrypt($this->input->post("user_id"))
        ];
        $this->load->model('User_model');
        $data['users_details'] = $this->User_model->get_user_details($filter);
        echo json_encode($data);
    }





    public function getAllApplicants()
    {
        $user_id = $this->session->user_id;
        $query = $this->Crud_model->getAllApplicants($user_id);
        $data = [];
        foreach ($query->result() as $k) {
            $data[] = array(
                "full_name" => $k->first_name = null ? "" : $k->first_name . ' ' . $k->middle_name = null ? "" : $k->middle_name . ' ' . $k->last_name = null ? "" : $k->last_name . ' ' . $k->suffix_name = null ? "" : $k->suffix_name,
                "job_title" => $k->job_title,
                "offer" => $k->offer = null ? "No Offer" : $k->offer,
                "job_type" => $k->job_type,
                "status" => $k->status,
                "id" => encrypt($k->users_id),
                "applyId" => encrypt($k->applyId),
            );
        }
        echo json_encode($data);
    }

    public function jobApplicants()
    {
        $data['job_key'] = $this->input->get("job");
        $this->load->view('template/jobapplicants_template', $data);
    }

    public function jobStatus()
    {  
        $data['notification'] = $this->input->get("notification");
        $this->load->view('template/job_status_template', $data);
    }

    public function getMyJobStatus()
    { 
        
            
        $where = [
            "notification.id" => decrypt(post("notification"))
        ];
        $data = [
            "status" => "Seen",
        ];
    
            $query = $this->Crud_model->update("notification",$data,$where);
        $query = $this->Crud_model->getNotification($where);
        $data = [];
        foreach($query->result() as $k)
        {
            $data[] = array(
                "company" => $k->company,
                "status" => $k->status,
                "job_title" => $k->job_title,
                "location" => $k->location,
                "job_key" => encrypt($k->job_key),
                
               
            );
        }
        echo json_encode($data);
    }

    public function getSpecificJobApplicants()
    {
        $user_id = $this->session->user_id;
        $job_key = decrypt(post("job_key"));
        $query = $this->Crud_model->getSpecificJobApplicants($user_id, $job_key);
        $data = [];
        foreach ($query->result() as $k) {
            $data[] = array(
                "full_name" => $k->first_name = null ? "" : $k->first_name . ' ' . $k->middle_name = null ? "" : $k->middle_name . ' ' . $k->last_name = null ? "" : $k->last_name,
                "job_title" => $k->job_title,
                "salary" => $k->salary == null ? "No Offer" : $k->salary,
                "job_type" => $k->job_type,
                "id" => encrypt($k->users_id),
                "applyId" => encrypt($k->applyId),
            );
        }
        echo json_encode($data);
    }

    public function viewApply()
    {
        $update = [
            "status" => "Seen",
            "updated_at" => date("y-m-d h:i:s"),
        ];
        $where = [
            "id" => decrypt(post("id")),
            "status" => ""
        ];
        $query = $this->Crud_model->update("apply", $update, $where);
    }


    public function approve()
    {
        $update = [
            "status" => "Approved",
            "updated_at" => date("y-m-d h:i:s"),
        ];
        $whereUserID = [
            "id" =>  decrypt(post("applyId")),
        ];
        $this->Crud_model->update("apply", $update, $whereUserID);

        $query = $this->Crud_model->select("apply","users_id,job_key",$whereUserID);
        $job_key = "";
        $users_id = "";
        foreach($query->result() as $k)
        {
            $job_key = $k->job_key;
            $users_id = $k->users_id;
        }
        $notifyData = [
            "users_id" => $users_id,
            "job_key" => $job_key,
            "target_user" => $users_id,
            "notification" => "Approved",
            "status" => "New"
        ];
        $notify = $this->Crud_model->notify($notifyData);

        $data["message"] = "You Successfuly Approve ";
        echo json_encode($query->result());
    }

   

    public function reject_applicant()
    {
        $update = [
            "status" => "Rejected",
            "updated_at" => date("y-m-d h:i:s"),
        ];
        $whereUserID = [
            "id" =>  decrypt(post("applyId")),
        ];
        $this->Crud_model->update("apply", $update, $whereUserID);

        $query = $this->Crud_model->select("apply","users_id,job_key",$whereUserID);
        $job_key = "";
        $users_id = "";
        foreach($query->result() as $k)
        {
            $job_key = $k->job_key;
            $users_id = $k->users_id;
        }
        $notifyData = [
            "users_id" => $users_id,
            "job_key" => $job_key,
            "target_user" => $users_id,
            "notification" => "Rejected",
            "status" => "New"
        ];
        $notify = $this->Crud_model->notify($notifyData);

        $data["message"] = "rejected";
        echo json_encode($data);
    }
}
