<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Crud_model");
      if(isset($this->session->user_id)) {
         redirect(base_url());
      } 
   }


   public function login()
   {
      $this->load->view('template/login_template');
   }

   public function register()
   {
      $this->load->view('template/register_template');
   }
}
