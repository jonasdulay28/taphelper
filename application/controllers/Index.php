<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

    public function __construct() {
        parent::__construct();
		date_default_timezone_set('Asia/Manila');

            $this->load->model("Crud_model"); 
    }


    public function index(){
        
        $this->load->view('template/homepage_template');
    }

    public function send_feedback() {
        $feedback_data = (array) post('feedback_data');
        $response = ["message"=>"success"];
        $this->Crud_model->insert('feedback_admin',$feedback_data);
        echo json_encode($response);
    }

    public function get_feedbacks() {
        $data['feedbacks'] = $this->Crud_model->fetch('feedback_admin');
        echo json_encode($data);
    }

    
}
