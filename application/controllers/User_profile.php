<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') or exit('No direct script access allowed');

class user_profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');
        $this->load->model("Crud_model");
    }

    public function index()
    {
        $this->load->view('template/user_profile_template');
    }

    public function uploadResume()
    {

        $response = ["message" => "success"];
        if (!empty($_FILES['file']['name'])) {
            // where are you storing it
            $path = './resume';
            // what are you naming it
            $new_name = 'resume-' . time();
            // start upload
            $input = 'file';
            $result = $this->upload_file($path, $new_name, $input);
            if ($result == "success") {
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $product_data['image'] = base_url() . "resume/" . $new_name . "." . $ext;
                $updata = [
                    "resume" => $new_name. "." .$ext,
                ];
                $where = [
                    "users_id" => $this->session->user_id,
                ];
                $query = $this->Crud_model->update("users_profile",$updata,$where);
            } else {
                $response["message"] = "failed2";
            }
        } else {
            $response["message"] = "failedd";
        }


        echo json_encode($response);
    }

    public function uploadProfilePic()
    {
        $response = ["message" => "success"];
        if (!empty($_FILES['file']['name'])) {
            // where are you storing it
            $path = './uploads';
            // what are you naming it
            $new_name = 'profpic-' . time();
            // start upload
            $input = 'file';
            $result = $this->upload_image($path, $new_name, $input);
            if ($result == "success") {
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $product_data['image'] = base_url() . "uploads/" . $new_name . "." . $ext;
                $updata = [
                    "profile_picture" => $new_name. "." .$ext,
                ];
                $where = [
                    "users_id" => $this->session->user_id,
                ];
                $query = $this->Crud_model->update("users",$updata,$where);
            } else {
                $response["message"] = "failed";
            }
        } else {
            $response["message"] = "failed";
        }


        echo json_encode($response);
    }

    function upload_file($path, $new_name, $input)
    {
        // define parameters
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $new_name;
        $this->load->library('upload');
        $this->upload->initialize($config);
        // upload the image
        if ($this->upload->do_upload($input)) {
            return "success";
        } else {
            return "failed";
        }
    }

    function upload_image($path, $new_name, $input)
    {
        // define parameters
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '0';
        $config['max_width'] = '0';
        $config['max_height'] = '0';
        $config['file_name'] = $new_name;
        $this->load->library('upload');
        $this->upload->initialize($config);
        // upload the image
        if ($this->upload->do_upload($input)) {
            return "success";
        } else {
            return "failed";
        }
    }

    public function getAllUserData()
    {
        $filter = [
            "id" => $this->session->user_id,
        ];
        $this->load->model('User_model');
        $data['users_details'] = $this->User_model->get_user_details($filter);
        echo json_encode($data);
    }

    public function update_user_profile()
    {
        $updata = post('user_profile');
        $new_password = clean_data(post('new_password'));
        $user_id =  $this->session->user_id;

        $usersData = [
            "first_name" => $updata['first_name'],
            "middle_name" => $updata['middle_name'],
            "last_name" => $updata['last_name'],
            "contact_number" => $updata['contact_number'],
            "address" => $updata['address'],
            "email_address" => $updata['email_address'],
        ];

        if($new_password) {
            $userData['new_password'] = hash_password($new_password);
        }
        $filter = ['id'=>$user_id];
        $this->Crud_model->update("users", $usersData, $filter);
        $msg["message"] = "Update Successfuly.";

        echo json_encode($msg);
    }
}
