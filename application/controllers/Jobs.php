<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') or exit('No direct script access allowed');

class jobs extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Manila');

        $this->load->model("Crud_model");
    }

    public function index() {
        $this->load->view('template/job_list_template');
    }

    public function apply_job()
    {
        $respnse = ["message" => "success"];
        $worker_id = $this->session->user_id;
        $job_key = clean_data(post('job_key'));
        $contact_number = post('contact_number');
        // $filter = ['worker_id'=>$worker_id,'status'=>''];
        // $check_exist = $this->Crud_model->check_exist('jobs',)
        sendSms("A worker applied to your service. Check it now!",$contact_number);
        $this->Crud_model->update('jobs', ['worker_id' => $worker_id, 'status' => "Confirmed"], ['job_key' => $job_key]);
        
        echo json_encode($respnse);
    }

    public function view_job()
    {
        $this->load->view('template/view_job_template');
    }

    public function get_job_details()
    {
        $user_id = $this->session->user_id;
        $job_key = clean_data(post('job_key'));
        $data['job_details'] = $this->Crud_model->get_job_details($job_key, $user_id);
        echo json_encode($data);
    }

    public function update_job() {
        $job_data = (array) post('jobpost_data');
        $address = clean_data($job_data['address']);
        $service_fee = clean_data($job_data['service_fee']);
        $rate = clean_data($job_data['rate']);
        $job_description = clean_data($job_data['job_description']);
        $status = clean_data($job_data['status']);
        $rating = clean_data($job_data['rating']);
        $landmark = clean_data($job_data['landmark']);
        $feedback = clean_data($job_data['feedback']);

        $data = ['address'=>$address,'service_fee'=>$service_fee,'rate'=>$rate,'job_description'=>$job_description,"status"=>$status,"feedback"=>$feedback,"rating"=>$rating];
        $this->Crud_model->update('jobs',$data,['job_key'=>$job_data['job_key']]);
        echo json_encode(['message'=>"success"]);
    }

    public function update_job_category() {
        $view_job_category = (array) post('view_job_category');
        $id =  $view_job_category['id'];
        unset($view_job_category['num_jobs']);

        $this->Crud_model->update('job_categories',$view_job_category,['id'=>$id]);
        echo json_encode(['message'=>"success"]);
    }


    

    public function job_categories()
    {
        $data['job_categories'] = $this->Crud_model->fetch("job_categories", "", "", "", "job_category asc");
        $data['popular_jobs'] = $this->Crud_model->fetch("job_categories", ['is_popular' => 1], "", "", "job_category desc");
        echo json_encode($data);
    }

    public function job_post()
    {
        $this->load->view('template/taphelper');
    }

    public function get_pending_jobs()
    {
        $data['jobs'] = $this->Crud_model->get_pending_jobs();
        echo json_encode($data);
    }

    public function add_job_category()
    {
        $add_job_category = (array)post('add_job_category');
        $this->Crud_model->insert('job_categories',$add_job_category);
        
        echo json_encode(['message'=>"success"]);
    }

    public function my_jobs() {
        $role = post('role');
        $user_id = $this->session->user_id;
        $data['jobs'] = $this->Crud_model->get_my_jobs($user_id,$role);
        echo json_encode($data);
    }

    public function jobProfile()
    {
        $this->load->view('template/taphelper');
    }

}
