<?php
header('Access-Control-Allow-Origin: *');  
defined('BASEPATH') or exit('No direct script access allowed');

class areas extends CI_Controller
{

   public function __construct()
   {
      parent::__construct();
      date_default_timezone_set('Asia/Manila');
      $this->load->model("Crud_model");
   }

   public function get_areas() {
      $data['areas'] = $this->Crud_model->fetch('areas',['status'=>1]);
      echo json_encode($data);
   }
}
