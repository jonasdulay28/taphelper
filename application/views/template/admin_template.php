<!DOCTYPE html>
<html>

<head>
   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139871344-1"></script>
   <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
         dataLayer.push(arguments);
      }
      gtag('js', new Date());

      gtag('config', 'UA-139871344-1');
   </script>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>TapHelper</title>
   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!-- Bootstrap 3.3.7 -->
   <link rel="stylesheet" href="<?php echo admin_folder(); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo admin_folder(); ?>bower_components/font-awesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="<?php echo admin_folder(); ?>bower_components/Ionicons/css/ionicons.min.css">
   <link rel="shortcut icon" type="image/x-icon" href="<?php echo images_bundle('favicon.png') ?>">
   <link rel="stylesheet" href="<?php echo admin_folder(); ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo admin_folder(); ?>dist/css/AdminLTE.min.css">
   <link rel="stylesheet" href="<?php echo styles_bundle('ladda.min.css') ?>">
   <link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
   <link href="//cdn.jsdelivr.net/npm/keyrune@latest/css/keyrune.css" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" href="<?php echo admin_folder(); ?>dist/css/skins/skin-blue.min.css">
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  <![endif]-->

   <!-- Google Font -->
   <!-- 
	  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
   <link rel="stylesheet" type="text/css" href="<?php echo styles_bundle('admin_global.css') ?>">
   <style type="text/css">
      html,
      body,
      .wrapper {
         height: 100% !important;
      }

      @media only screen and (max-width: 1300px) {
         .table-responsive {
            display: block;
         }
      }
   </style>
   <script type="text/javascript">
      var base_url = "<?php echo base_url() ?>";
   </script>
</head>

<body class="hold-transition sidebar-mini" >
      
      
   <div id="app"></div>
   
   <!-- jQuery 3 -->
   <script type="text/javascript">
      function labelFormatter(label, series) {
         return '<div style="font-size:13px; text-align:center; padding:2px; color: #fff; font-weight: 600;">' +
            label +
            '<br>' +
            Math.round(series.percent) + '%</div>'
      }
   </script>
   <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
   <!-- Bootstrap 3.3.7 -->
   <script src="<?php echo admin_folder(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
   <script src="<?php echo admin_folder(); ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
   <script src="<?php echo admin_folder(); ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
   <!-- 
		<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script> -->
   <script src="<?php echo scripts_bundle('spin.min.js') ?>"></script>
   <script src="<?php echo scripts_bundle('ladda.min.js') ?>"></script>
   <script src="<?php echo scripts_bundle('bootstrap-select.min.js') ?>"></script>
   <!-- FastClick -->
   <!-- AdminLTE App -->
   <script src="<?php echo admin_folder(); ?>dist/js/adminlte.min.js"></script>
   <script src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
   <!-- AdminLTE for demo purposes -->
   <script src="<?php echo admin_folder(); ?>dist/js/demo.js"></script>
   <script src="<?php echo admin_folder(); ?>bower_components/Flot/jquery.flot.js"></script>
   <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
   <script src="<?php echo admin_folder(); ?>bower_components/Flot/jquery.flot.resize.js"></script>
   <!-- FLOT PIE PLUGIN - also used to draw donut charts -->
   <script src="<?php echo admin_folder(); ?>bower_components/Flot/jquery.flot.pie.js"></script>
   <!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
   <script src="<?php echo admin_folder(); ?>bower_components/Flot/jquery.flot.categories.js"></script>
   <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.19/sorting/currency.js"></script>
   <script type="text/javascript" src="<?php echo scripts_bundle('global.js') ?>?random=<?php echo uniqid(); ?>"></script>
   <script type="text/javascript" src="<?php echo base_url() ?>assets/src/dist/js/admin.js?random=<?php echo uniqid(); ?>"></script>
</body>

</html>