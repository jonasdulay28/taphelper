<!DOCTYPE html>
<html lang="en">

<head>
  <title>TapHelper</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
  <link rel="stylesheet" href="fonts/icomoon/style.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo styles_bundle('sweetalert2.min.css') ?>">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/flaticon/font/flaticon.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
</head>

<body>
  <div class="site-wrap">
    <?php
    $this->load->view('pages/view_job');
    ?>
    <!-- <div style="height: 113px;"></div> -->
    <script>
      var base_url = "<?php echo base_url() ?>";
      var job_key = "<?php echo $this->uri->segment(2);?>";
    </script>
    <script src="<?php echo base_url() ?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo scripts_bundle('sweetalert2.min.js') ?>"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url() ?>assets/jss/global.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/src/dist/js/app.js?random=<?php echo uniqid(); ?>"></script>
</body>

</html>