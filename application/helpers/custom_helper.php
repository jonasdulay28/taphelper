<?php
defined('BASEPATH') or exit('No direct script access allowed');

// ------------------------------------------------------------------------


function numtowords($x)
{

    $oneto19 = array("", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN");
    $tens = array("", "", "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY");
    $hundreds = array("", "ONE HUNDRED", "TWO HUNDRED", "THREE HUNDRED", "FOUR HUNDRED", "FIVE HUNDRED", "SIX HUNDRED", "SEVEN HUNDRED", "EIGHT HUNDRED", "NINE HUNDRED");
    $scale = array("", "", "THOUSAND", "MILLION");

    $num = number_format($x, 2, ".", ",");
    $num_arr = explode(".", $num);

    $numwords = "";

    $wholenum = $num_arr[0];
    $decnum = $num_arr[1];
    $whole_arr = explode(",", $wholenum);

    $whole_count = count($whole_arr);
    $scaleCounter = $whole_count;
    for ($i = 0; $i < $whole_count; $i++) {
        $digitcount = strlen($whole_arr[$i]);

        switch ($digitcount) {
            case "3":
                $digit3 = substr($whole_arr[$i], 0, 1);
                $whole_arr[$i] = $whole_arr[$i] - ($digit3 * 100);
                $numwords .= "" . $hundreds[$digit3] . " ";
            case "2":
                $digit2 = substr($whole_arr[$i], 0, 1);
                if ($digit2 < 2) {
                    $digit1 = substr($whole_arr[$i], 1, 1);
                    $digit21 = $digit2 . "" . $digit1;
                    $numwords .= $oneto19[$digit21] . " ";
                    break;
                } else {
                    $digit2 = substr($whole_arr[$i], 0, 1);
                    $whole_arr[$i] = $whole_arr[$i] - ($digit2 * 10);
                    $numwords .= $tens[$digit2] . " ";
                }
            case "1":
                $digit1 = substr($whole_arr[$i], 0, 1);
                $whole_arr[$i] = $whole_arr[$i] - ($digit1 * 1);
                $numwords .= $oneto19[$digit1] . " ";

                break;
        }
        $numwords .= $scale[$scaleCounter] . " ";
        $scaleCounter--;
    }
    if ($decnum > 0) {
        $numwords .= " PESOS and ";
        if ($decnum < 20) {

            if ($decnum < 10) {
                $decnum = str_replace(0, "", $decnum);
                $numwords .= $oneto19[$decnum];
            } else {
                $numwords .= $oneto19[$decnum];
            }
        } elseif ($decnum < 100) {
            $numwords .= $tens[substr($decnum, 0, 1)];
            $numwords .= " " . $oneto19[substr($decnum, 1, 1)];
        }
        $numwords .= ' ' . 'CENTS';
    } else {
        $numwords .= 'PESOS';
    }



    return $numwords;
}


// if ( ! function_exists('monthDiff')) {

//     function monthDiff($lastMonth,$lastYear){	

//         $diff = ((date("Y")- $lastYear)*12)+(date("m") - $lastMonth);
//         return $diff;
//     }
// }

// if ( ! function_exists('penalty'))
// {
//     function penalty($payment,$penaltyPercentage,$monthDiff){
//         $monthDiff = $monthDiff*$penaltyPercentage;
//         $mDiff = "";
//         if($monthDiff>72)
//         {
//             $mDiff = 72;
//         }
//         else{
//             $mDiff = $monthDiff;
//         }
//         $penalty = ($payment * ($mDiff /100));
//         return $penalty;
//     }
// }

// if ( ! function_exists('discount'))
// {
//     function discount($payment){
//         $penalty = ($payment * (10 /100)*(-1));
//         return $penalty;
//     }
// }

// if ( ! function_exists('saveMoney'))
// {
//      function saveMoney($money){
//         return str_replace(",","",$money);
//     }
// }

// if ( ! function_exists('showMoney')){
//      function showMoney($money){
//         return number_format($money,2);
//     }
// }

if (!function_exists('clean_data')) {
    function clean_data($value)
    {
        $value = trim($value);
        $value = str_replace('\\', '', $value);
        $value = strtr($value, array_flip(get_html_translation_table(HTML_ENTITIES)));
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        return $value;
    }
}
if (!function_exists('flag')) {
    function flag($flag, $defaultValue)
    {
        $flag_value = !empty($_GET[$flag]) ? $_GET[$flag] : (!empty($_COOKIE[$flag]) ? $_COOKIE[$flag] : $defaultValue);
        setcookie($flag, $flag_value, time() + (86400 * 30));
        return $flag_value;
    }
}

if (!function_exists('styles_bundle')) {
    function styles_bundle($style)
    {
        return base_url('assets/css/' . $style);
    }
}

if (!function_exists('images_bundle')) {
    function images_bundle($image)
    {
        return base_url('assets/images/' . $image);
    }
}

if (!function_exists('scripts_bundle')) {
    function scripts_bundle($script)
    {
        return base_url('assets/js/' . $script);
    }
}

if (!function_exists('material_scripts')) {
    function material_scripts()
    {
        return base_url('material/assets/js/');
    }
}
if (!function_exists('material_styles')) {
    function material_styles()
    {
        return base_url('material/assets/css/');
    }
}

if (!function_exists('third_party_bundle')) {
    function third_party_bundle()
    {
        return base_url('assets/third_party/');
    }
}

if (!function_exists('pad')) {
    function pad($num, $size)
    {
        $s = $num . "";
        while (strlen($s) < $size) $s = "0" . $s;
        return $s;
    }
}

if (!function_exists('post')) {
    function post($name)
    {
        $controller = &get_instance();
        return $controller->input->post($name);
    }
}


if (!function_exists('get')) {
    function get($name)
    {
        $controller = &get_instance();
        return $controller->input->get($name);
    }
}

if (!function_exists('encrypt')) {

    function encrypt($string)
    {
        $output = FALSE;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '$6$/!@#$%$3cr3tk3y%$#@!';
        $secret_iv = '$6$/!@#$%$3cr3tIV%$#@!';

        // hash
        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;
    }
}

if (!function_exists('admin_folder')) {
    function admin_folder()
    {
        return base_url('admin/');
    }
}

if (!function_exists('get_email')) {
    function get_email()
    {
        $ci = &get_instance();
        $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : $ci->session->ses_key;
        $email = "";
        if ($cookie_key != '')
            $email = decrypt($cookie_key);
        return $email;
    }
}

if (!function_exists('get_username')) {
    function get_username()
    {
        $ci = &get_instance();
        $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : $ci->session->ses_key;
        $username = strstr(decrypt($cookie_key), '@', true);;
        return $username;
    }
}

function sendSms($body, $contact_number)
   {
      $ch = curl_init();
      $url = "https://smsgateway24.com/getdata/addsms";

      $postarray = [
         'token' => '5189695f901de9c2c84a07dab422ccf2',
         'sendto' => $contact_number,
         'body'   => $body,
         'device_id'   => 2800,
         'sim' => 1
      ];

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postarray);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
      $output = curl_exec($ch);
      curl_close($ch);
      return $output;
}



if (!function_exists('hash_password')) {
    function hash_password($password)
    {
        $options = array(
            'cost' => 11,
        );
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }
}

function random_code($limit)
{
    return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
}

if (!function_exists('decrypt')) {

    function decrypt($string)
    {
        $output = FALSE;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '$6$/!@#$%$3cr3tk3y%$#@!';
        $secret_iv = '$6$/!@#$%$3cr3tIV%$#@!';
        // hash
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }
}

function in_arrayi($needle, $haystack)
{
    return in_array(strtolower($needle), array_map('strtolower', $haystack));
}

if (!function_exists('clean_data')) {
    function clean_data($string)
    {
        return trim(strip_tags(stripslashes($string)));
    }
}

if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}



if (!function_exists('is_logged')) {
    function is_logged()
    {
        $ci = &get_instance();
        $cookie_key = !empty($_COOKIE['ses_key']) ? $_COOKIE['ses_key'] : '';
        $cookie_pass = !empty($_COOKIE['ses_pass']) ? $_COOKIE['ses_pass'] : '';
        $ci->load->model('User_model');
        if ($ci->session->ses_key != "") {
            $email = decrypt($ci->session->ses_key);
            $filter = ["email" => $email];
            $status = $ci->User_model->check_exist('users', $filter);
            if ($status)
                return 1;
            else
                return 0;
        } else {
            if ($cookie_key != "" && $cookie_pass != "") {
                $email = decrypt($cookie_key);
                $password = decrypt($cookie_pass);
                $filter = ["email" => $email];
                $row = $ci->User_model->fetch_tag_row('password', 'users', $filter);
                if (empty($row)) {
                    return 0;
                } else {
                    $user_password = $row->password;
                    if (password_verify($password, $user_password)) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            } else {
                return 0;
            }
        }
    }
}

if (!function_exists('is_logged_admin')) {
    function is_logged_admin()
    {
        $ci = &get_instance();
        $ci->load->model('User_model');
        if ($ci->session->ses_key != "") {
            $email = decrypt($ci->session->ses_key);
            $filter = ["email" => $email, "role" => "admin"];
            $status = $ci->User_model->check_exist('users', $filter);
            if ($status)
                return 1;
            else
                return 0;
        } else {
            return 0;
        }
    }
}
